﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //start the progam clear();
            Console.Clear();


            //end the program with blankinstructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine ("Press any key to close");
            Console.ReadKey();
        }
    }
}

